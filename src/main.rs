extern crate rand;
extern crate byteorder;
use std::net::*;
use std::fmt;
#[macro_use]
extern crate clap;

use std::io::Cursor;
use byteorder::{BigEndian, ReadBytesExt};
use clap::App;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

// stucts begin
struct IPNetwork {
    pub network_address: Ipv4Addr,
    pub netmask: Ipv4Addr,
}

impl fmt::Debug for IPNetwork {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/{}", self.network_address, self.netmask)
    }
}

impl IPNetwork {
    fn new(address: Ipv4Addr, netmask: Ipv4Addr) -> IPNetwork {
        let ip_octets = address.octets();
        let mut buf = Cursor::new(&ip_octets[..]);
        let binip = buf.read_u32::<BigEndian>().unwrap();
        let mask_octets = netmask.octets();
        let mut buf = Cursor::new(&mask_octets[..]);
        let binmask = buf.read_u32::<BigEndian>().unwrap();
        IPNetwork {
            network_address: Ipv4Addr::from(binip & binmask),
            netmask: netmask,
        }
    }
    fn prefix(&self) -> u8 {
        let mask_octets = self.netmask.octets();
        let mut buf = Cursor::new(&mask_octets[..]);
        let mut mask = buf.read_u32::<BigEndian>().unwrap();
        let mut prefix: u8 = 0;
        let mut done = false;
        let mut cnt = 0;
        while !done {
            if (mask & 0x1) == 0 {
                mask = mask >> 1;
                cnt = cnt + 1;
            } else {
                prefix = 32 - cnt;
                done = true
            }
        }
        prefix
    }
}

fn mask_from_prefix(prefix: u8) -> Ipv4Addr {
    let binip = match prefix {
        0 => 0xffffffff - 1,
        _ => !((1 << (32 - prefix)) - 1),
    };
    Ipv4Addr::from(binip)
}

fn generate_random_ip() -> Ipv4Addr {
    Ipv4Addr::new(10,
                  rand::random::<u8>(),
                  rand::random::<u8>(),
                  rand::random::<u8>())
}

fn main() {
    let matches = App::new("iprandomizer")
        .version(VERSION)
        .author("Marcin Jurczuk <mjurczuk@gmail.com>")
        .about("Random IP generator")
        .args_from_usage("-p, --prefix [prefix] 'Generated network prefix length'
                                         \
                          -d... 'Turn debugging information on'")
        .get_matches();

    // parse command line
    let prefix = value_t!(matches, "prefix", u8).unwrap_or(28);
    // main flow
    let ip = generate_random_ip();
    let mynet = IPNetwork::new(ip, mask_from_prefix(prefix));
    println!("IP: {:?}\nNetwork: {:?} ({})", ip, mynet, mynet.prefix());
}
